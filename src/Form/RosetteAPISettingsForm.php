<?php

namespace Drupal\rosette_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use rosette\api\Api;

/**
 * Configure Rosette API settings for this site.
 */
class RosetteAPISettingsForm extends ConfigFormBase {

  /**
   * Constructs a \Drupal\rosette_api\Form\RosetteAPISettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rosette_api_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rosette_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('rosette_api.settings');

    $form['rosette_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rosette key'),
      '#default_value' => ($config->get('rosette_key')) ? $config->get('rosette_key') : '',
      '#required' => TRUE,
      '#size' => 60,
      '#maxlength' => 32,
      '#description' => $this->t('The contents of the Rosette API key. See <a href="@url">Rosette API documentation</a> to create a set up authentication.', [
        '@url' => 'https://developer.rosette.com/guide-start',
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Ping Rosette API
    try {
      $keyFileData = $form_state->getValue('rosette_key');
      $api = new Api($keyFileData);
      $api->ping();
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('project_id', $this->t('Rosette API test failed. Please enter correct key'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('rosette_api.settings');
    $config->set('rosette_key', $form_state->getValue('rosette_key'));
    $config->save();
    parent::submitForm($form, $form_state);
  }
}
