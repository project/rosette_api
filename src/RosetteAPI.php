<?php

namespace Drupal\rosette_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Psr\Log\LoggerInterface;
use rosette\api\Api;

/**
 * Establishes a connection to Rosette API.
 */
class RosetteAPI {

  use StringTranslationTrait;

  /**
   * The rosette_api.settings config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs an Rosette API object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, TranslationInterface $string_translation) {
    $this->config = $config_factory->get('rosette_api.settings');
    $this->logger = $logger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Logs an error and throws an exception.
   *
   * @param string $message
   *   The error message.
   * @param array $context
   *   Any parameters needed in order to build the error message.
   *
   * @throws \Exception
   */
  private function handleError($message, array $context = []) {
    $this->logger->error($message, $context);
    throw new \Exception($this->t($message, $context));
  }
}
